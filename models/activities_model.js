const Mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  },
  toObject: {
    virtuals: true
  }
};

var activitySchema = new Mongoose.Schema({
  username: { type: String },
  action: { type: String },  
  date: {type: Date, default: Date.now},
}, schemaOptions)


// astrid_activities adalah nama schema / tabel di MangoDB-nya
const activity = Mongoose.model('astrid_activities', activitySchema);
module.exports = activity;