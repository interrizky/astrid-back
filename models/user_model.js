const Mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  },
  toObject: {
    virtuals: true
  }
};

var userSchema = new Mongoose.Schema({
  username: { type: String },
  password: { type: String },
  role: { type: String },  
  status: { type: String },
  question: { type: String },
  answer: { type: String },
  email: { type: String },
  name: { type: String }
}, schemaOptions)


// astrid_users adalah nama schema / tabel di MangoDB-nya
const user = Mongoose.model('astrid_users', userSchema);
module.exports = user;