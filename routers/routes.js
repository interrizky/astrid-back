const express = require('express')
const routes = express.Router()

const loginController = require('../controllers/login_controller')

routes.post('/auth', loginController.auth)
routes.post('/checkUname', loginController.checkUname)
routes.post('/chgpwd', loginController.chgPwd )
routes.get('/fetchActivities/:username', loginController.fetchActivities)

/* fungsi release */
routes.post('/bukisys_rel', loginController.bukisys_rel)
routes.post('/bukidesk_rel', loginController.bukidesk_rel)
routes.post('/cls_rel', loginController.cls_rel)
routes.post('/silverlake_rel', loginController.silverlake_rel)
routes.post('/swamitra_rel', loginController.swamitra_rel)
routes.post('/display_rel', loginController.display_rel)

/* fungsi inquiry */
routes.post('/inquiry', loginController.inquiry)

/* dropdown user cabang */
routes.get('/dropdown_user', loginController.dropdown_user)

/* retrieve tabel di page "Report" */
routes.post('/retrieval', loginController.retrieval)

//Export to index.JS
module.exports = routes
