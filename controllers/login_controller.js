const express = require('express')
const JWT = require('jsonwebtoken')
const userModel = require('../models/user_model')
const activityModel = require('../models/activities_model')
const { ConnectionClosedEvent } = require('mongodb')

exports.auth = async(req, res) => {
  if( !req.body.username || req.body.username == '' || !req.body.password || req.body.password == '' ) {
      res.status(401).send({ message: 'Typo Username or Password?' })
  } else {
      const userData = await userModel.findOne( {'username': req.body.username, 'password': req.body.password} )

      if( userData === null || userData === 'undefined' ) {
          res.status(401).send({
            message: "Hey, Invalid username or password",
            result: "Hey, Invalid username or password"
          })      
      } else {
          let token = JWT.sign( {
            username: userData.username,
            email: userData.email,
          }, 'myPrivateKey', { expiresIn: '1D' } );

          let passingData = {
            uid: userData._id,
            username: userData.username,
            email: userData.email,
            role: userData.role,
            status: userData.status,
            name: userData.name,
            token: token,
            token_type: 'Bearer'
          }

          res.status(200).send({
            message: "Login Accepted",
            result: passingData
          })
      }      
  }
}

exports.checkUname = async(req, res) => {
  const datax = await userModel.findOne( { username: req.body.username } )

  if( datax == null) {
    res.status(200).send({
      message: "No Results Found",
      result: "No Results Found"
    })
  } else {
    if( datax.status == 'inactive' ) {
      res.status(200).send({
        message: "Prohibited",
        result: datax
      })
    } else {
      res.status(200).send({
        message: "Fetch Data Succeed",
        result: datax      
      })
    }    
  }
}

exports.fetchActivities = async(request, response) => {
  const activityData = await activityModel.find( {'username': request.params.username} ).sort({_id: -1}).limit(5)

  if( activityData !== null || activityData !== undefined ) {
    response.status(200).send({
      message: "Fetch Data Succeed",
      result: activityData
    }) 
  } else {
    response.status(204).send({
      message: "No Results Found",
      result: "No Results Found"
    })
  }
}

exports.chgPwd = async(req, res) => {
  const filter = { username: req.body.data_uname };
  const update = { password: req.body.data_pass };

  // `doc` is the document _after_ `update` was applied because of
  // `returnOriginal: false`
  const doc = await userModel.findOneAndUpdate(filter, update, {returnOriginal: false})

  if( doc !== null || doc !== undefined ) {
    res.status(200).send({
      message: "Fetch Data Succeed",
      result: "Success"
    })
  } else {
    res.status(204).send({
      message: "Update Failed",
      result: "Update Failed"
    })
  }
}

exports.bukisys_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release AS400/Bukisys User Failed",
      result: "Release AS400/Bukisys User Failed"
    })
  }
}

exports.bukidesk_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release Bukidesk User Failed",
      result: "Release Bukidesk User Failed"
    })
  }
}

exports.cls_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release CLS User Failed",
      result: "Release CLS User Failed"
    })
  }
}

exports.silverlake_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release Silverlake User Failed",
      result: "Release Silverlake User Failed"
    })
  }
}

exports.swamitra_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release Swamitra User Failed",
      result: "Release Swamitra User Failed"
    })
  }
}

exports.display_rel = async(req, res) => {
  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_release,
    date: Date.now()
  })

  const posting = await dataOptions.save();
  // console.log(posting)

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Data Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Release Display Failed",
      result: "Release Display Failed"
    })
  }
}

exports.dropdown_user = async(req, res) => {
  // const list_user = await userModel.find( {'role': 'user'} )
  const list_user = await userModel.find({ $or: [{'role': 'admin'}, {'role': 'user'}] })
  // console.log(list_user)

  if( list_user !== null ) {
    res.status(200).send({
      message: 'Success To Pull Data User',
      result: list_user
    })
  } else {
    res.status(204).send({
      message: 'Failed To Load Data User',
      result: 'Failed To Load Data User',
    })
  }
}

exports.retrieval = async(req, res) => {
  const datax = await activityModel.find({ 'username': req.body.username, 'date': { $gte: req.body.tgl_awal, $lte: req.body.tgl_akhir } }).then(response => response).catch(error => false)

  if( datax !== null ) {
    res.status(200).send({
      message: "Success To Fetch Data",
      result: datax
    })
  } else {
    res.status(204).send({
      message: "Failed To Fetch Data",
      result: "Failed To Fetch Data"
    })    
  }
}

exports.inquiry = async(req, res) => {
  // console.log(req.body)

  const dataOptions = new activityModel({
    username: req.body.username,
    action: req.body.data_inquiry,
    date: Date.now()
  })

  const posting = await dataOptions.save();

  if( posting !== null || posting === undefined ) {
    res.status(200).send({
      message: "Submit Inquiry Succeed",
      result: posting
    })
  } else {
    res.status(204).send({
      message: "Inquirty Request Failed",
      result: "Inquirt Request Failed"
    })
  }
}